// Copyright 2014-2023 YeHaike. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UEUDPDemoTarget : TargetRules
{
	public UEUDPDemoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "UEUDPDemo" } );
	}
}
