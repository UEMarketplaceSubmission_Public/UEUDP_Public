// Copyright 2014-2023 YeHaike. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UEUDPDemoEditorTarget : TargetRules
{
	public UEUDPDemoEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

        bLegacyPublicIncludePaths = false;
        DefaultBuildSettings = BuildSettingsVersion.V2;

        IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
        ExtraModuleNames.AddRange( new string[] { "UEUDPDemo" } );
	}
}
