

# PRODUCT TITLE：

**UE-UDP(UEUDP, BlueprintUDP, UDP Feature For UE)**

**UE-UDP**: or named **UEUDP**, Unreal Engine UDP. 

**UE Marketplace "UE-UDP" Plugin:** https://www.unrealengine.com/marketplace/en-US/product/ue-udp-ueudp-blueprintudp-udp-feature-for-ue

![image-20230812102748325](README/00_Res/01_Images/image-20230812102748325.png)

![GALLERY_00_02](README/00_Res/01_Images/GALLERY_00_02.png)

# SHORT DESCRIPTION：

Only need to use the blueprints, you can send and receive UDP (User Datagram Protocol) messages. It is cross-platform and supports Unicode(UTF8) characters. The most important thing is that it is very easy to use.



# LONG DESCRIPTION：

This plugin encapsulates the implementation of UDP. Only need to use the blueprints, you can send and receive UDP messages. This plugin supports multiple platforms: iOS, Windows, Mac, etc. It supports Unicode(UTF8) characters. After using this plugin, you will find that it is so simple to receive and send UDP messages. In addition, this plugin also provides additional Wake-on-LAN functionality.

## Features: 

-  Send and receive UDP messages
-  Only use the blueprints
-  Cross-Platform
-  Very easy to use
-  Support for Unicode(UTF8) characters



# Example Introduction

**Example Main Map:** '`/Game/UEUDPDemo/Maps/MAP_UEUDPDemo_Overview`'

![image-20230805223925941](README/00_Res/01_Images/image-20230805223925941.png)

All Blueprint nodes and example code can be found in the Level Blueprint of this level.

![BPGraphScreenshot_2023Y-08M-05D-22h-33m-00s-178_00](README/00_Res/01_Images/BPGraphScreenshot_2023Y-08M-05D-22h-33m-00s-178_00.png)

## Blueprint nodes overview of "UE-UDP":

![image-20230805215950741](README/00_Res/01_Images/image-20230805215950741.png)

**"1":** Create a UEUDP Object.

**"2":** Send UDP Message Bytes to a specified IPAddress(with Port).

**"3":** Send UDP Message String to a specified IPAddress(with Port).

![image-20230805220646818](README/00_Res/01_Images/image-20230805220646818.png)

**"1":** Enable broadcasting, UDP messages will be sent to a specified range of IP addresses. For example, the IP addresses can be in the following range format: "255.255.255.255:567".

**"2":** Enable Multicast Loopback of UDP. Multicast Loopback enables a process to send UDP multicast messages on the same host and receive these messages from other processes on the same host. This allows multiple processes on the same host to interact in UDP multicast communication without the need for real network interfaces to send and receive multicast messages.

**"3":**  Enable Multicast Ttl of UDP. In UDP, Multicast Ttl is a parameter used to control the scope of multicast message transmission. TTL stands for "Time To Live," which is an 8-bit field typically included in the header of UDP packets.

The purpose of Multicast Ttl is to limit the number of hops or jumps a multicast message can traverse in the network. Each time a multicast message passes through a router, the TTL value is decremented by 1. When the TTL value reaches 0, the router will no longer forward the message and will discard it. This prevents multicast messages from propagating indefinitely in the network, avoiding network congestion and resource wastage.

By appropriately setting the Multicast Ttl value, the scope of multicast message transmission in the network can be controlled. For instance, setting the TTL value to 1 confines the multicast message to the local subnet and prevents it from crossing routers. If the TTL value is set to a value greater than 1, the multicast message can be transmitted across multiple subnets, but the number of hops will be limited.

**"4":** Begin receive UDPMessage.

**"5":** Bind Event to "OnReceivedNewUDPMessageMulticastDelegate".

![image-20230806124239751](README/00_Res/01_Images/image-20230806124239751.png)

**"1":** Converts string to bytes.

**"2":** Converts bytes to string.

## WakeOnLan Feature

Functions related to waking up a computer over a local area network.

/Script/Engine.Blueprint'/UEUDP/MODULEs/WakeOnLAN/BPs/BFLs/BFL_WakeOnLan.BFL_WakeOnLan'

![image-20230806120128413](README/00_Res/01_Images/image-20230806120128413.png)

/Script/Engine.Blueprint'/UEUDP/ASSETs/BPs/BFLs/BFL_UEConverter.BFL_UEConverter'

![image-20230806120018089](README/00_Res/01_Images/image-20230806120018089.png)



## Explanation of the example blueprint code:

### 1. First: create one UEUDP Object.

![image-20230806115630648](README/00_Res/01_Images/image-20230806115630648.png)

### 2. Bind Event to "OnReceivedNewUDPMessageMulticastDelegate". When receiving a UDP message, print the UDP string message and the sender's IP and port information.

![image-20230806115549506](README/00_Res/01_Images/image-20230806115549506.png)

### 3. After running (e.g., PIE, play in editor), press the "1" key on the keyboard to send a specified string message (which may contain Unicode(UTF8) characters) to the local machine. Press the "2" key on the keyboard to broadcast a specified bytes message (converted from a String, may contain Unicode(UTF8) characters) to a specific range of IP addresses.

![image-20230806115738221](README/00_Res/01_Images/image-20230806115738221.png)

### 4. Press the "3" key on the keyboard to send a Magic Packet to the specified IP address of a host, used to wake up a host in a powered-off state over the local network.

**Function "Convert(MACAddressToMagicPacket)"**: Convert the MACAddress of the target host to the MagicPacket.

![image-20230806115821828](README/00_Res/01_Images/image-20230806115821828.png)

**NOTE01:**  To wake up a computer in a powered-off state over the network, the common method is to send a "Magic Packet," which is a special type of data packet. The process of waking up the host requires the following conditions:

1. The host's network adapter (NIC) supports Wake-on-LAN (WOL) functionality and is properly configured. Most modern network adapters support this feature.
2. The host is in a powered-off or sleeping state, not completely powered down.
3. The host's BIOS or UEFI settings have Wake-on-LAN functionality enabled.
4. Both the sender and receiver of the Magic Packet are within the same local area network (LAN) and are not blocked by network devices like routers or switches.



# NOTEs:

## 01 Solutions for "The String ends with garbled characters".

To prevent garbled characters at the end of the String (such as the "NewUDPMessageString" parameter of "OnReceivedNewUDPMessageMulticastDelegate"), Bytes sent via UDP must end with the '\0' character.

### Example: Use the "OnReceivedNewUDPMessageMulticastDelegate" to receive UDP messages.

![image-20230815005511628](README/00_Res/01_Images/image-20230815005511628.png)

#### **Solution01**: Send the Bytes with '\0' (uint8 value is 0) at the end. 

**If Bytes not end with '\0', the "NewUDPMessageString" maybe end with garbled characters.**

![image-20230815005538141](README/00_Res/01_Images/image-20230815005538141.png)

**If Bytes end with '\0', the "NewUDPMessageString" will not end with garbled characters.**

![image-20230815005637696](README/00_Res/01_Images/image-20230815005637696.png)

#### **Solution02**: Add '\0' (uint8 value is 0) to the end of received Bytes. Then use the "Convert bytes to string" function to convert Bytes to String..

![image-20230815010756886](README/00_Res/01_Images/image-20230815010756886.png)
