// Copyright 2014-2023 YeHaike. All Rights Reserved.

#include "UEUDPDemo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UEUDPDemo, "UEUDPDemo" );
